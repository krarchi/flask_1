from flask import *
import pymysql
import base64

db = pymysql.connect("localhost","root","1234","users")
cursor = db.cursor()

app = Flask(__name__)

@app.route('/')
def home():
    return render_template("Login.html")

@app.route('/rr')
def rr():
    return render_template("Registration.html")

@app.route('/welcome')
def welcome():
    return "Welcome User- You have logged in successfully"

@app.route('/login',methods=['POST'])
def login():
    try:
        username = request.form['username']
        password = request.form['password']
        sql = "Select * from content where email = \'%s\' ;"%(username)
        cursor.execute(sql)
        data = cursor.fetchone()
        pas = base64.b64decode(data[1])
        if password == pas:
            return redirect(url_for('welcome'))
        else:
            print "Incorrect password"
            return redirect(url_for('failed'))
    except Exception as e:
        print e
        return redirect(url_for('failed'))
@app.route('/register',methods=['POST'])
def register():
    try:
        username = request.form['email']
        password = request.form['psw']
        r_password = request.form['psw-repeat']
        if password == r_password:
            password = base64.b64encode(r_password)
            sql = "Insert into content values(\'%s\',\'%s\');"%(username,password)
            cursor.execute(sql)
            db.commit()
            return "You have registered Successfully"
        else:
            print "Password didn't match"
            return redirect(url_for('failed'))
    except Exception as e:
        print e
        return redirect(url_for('failed'))
@app.route('/failed')
def failed():
    return "Your attempt failed / goto:- http://127.0.0.1:5000/"

app.run()